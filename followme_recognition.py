#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Follow me,音声班,音声認識
# 暫定版

import rospy
from std_msgs.msg import String, Bool
import os
from pocketsphinx import LiveSpeech

pub = rospy.Publisher('speech', String, queue_size=10) # 音声認識の結果をやり取りする
rospy.init_node('talker', anonymous=True)
speech_recognition = True

def talker():
    model_path = '/usr/local/lib/python2.7/dist-packages/pocketsphinx/model'
    while 1:
        global speech_recognition
        if speech_recognition == True:
            #print('true')
            speech = LiveSpeech(
            verbose=False,
            sampling_rate=8000,
            buffer_size=2048,
            no_search=False,
            full_utt=False,
            hmm=os.path.join(model_path, 'en-us'),
            lm=os.path.join(model_path, 'en-us.lm.bin'),
            dic=os.path.join(model_path, 'follow_me.dict') # 使う辞書指定する
            )

        # 音声認識ストップ
        if speech_recognition == False:
            #print('false')
            speech = LiveSpeech(
            verbose=False,
            sampling_rate=8000,
            buffer_size=2048,
            no_search=True,
            full_utt=False,
            hmm=os.path.join(model_path, 'en-us'),
            lm=os.path.join(model_path, 'en-us.lm.bin'),
            dic=os.path.join(model_path, 'follow_me.dict')
            )

        if speech_recognition == True:
            for text in speech:
                text = str(text)
                print(text)
                pub.publish(text) # 音声認識の結果をpublish
                break
        if speech_recognition == False:
            while speech_recognition != True:
                pass
        continue

# 音声認識再開のメッセージを受け取る関数
def control(data):
    global speech_recognition
    speech_recognition = data.data
    print(speech_recognition)
# 音声認識停止のメッセージを受け取る関数
def control_stop(data):
    global speech_recognition
    speech_recognition = data.data
    print(speech_recognition)


if __name__ == '__main__':
    rospy.Subscriber('recognition_start', Bool, control) # 音声認識再開を受け取る
    rospy.Subscriber('recognition_stop', Bool, control_stop) # 音声認識ストップを受ける取る
    talker()
