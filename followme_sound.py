#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Follow me,音声班,発話,ビープ音
# 暫定版

import os
import rospy
from std_msgs.msg import String
import subprocess

def callback(data):
    answer = data.data
    subprocess.call("aplay /home/shinonome/catkin_ws/src/spr_sphinx/src/stop.wav", shell=True) # ビープ音
    os.system('espeak "{}" -s 135'.format(answer)) # 発話
    pub_finish.publish('speaker')
    subprocess.call("aplay /home/shinonome/catkin_ws/src/spr_sphinx/src/start.wav", shell=True) # ビープ音

def listener():
    rospy.Subscriber('speaker', String,callback) # 発話する答えの文を受け取る
    rospy.init_node('listener', anonymous=True)
    rospy.spin()

if __name__ == '__main__':
    pub_finish = rospy.Publisher('finish', String, queue_size=10) # 発話が終了したことを知らせる
    listener()
