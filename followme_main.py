#!/usr/bin/python
# coding:utf-8
# Follow me,mainノード
# 暫定版

import rospy
from std_msgs.msg import String, Bool
import time
import os # espeakで使う
import difflib # 類似度を計算するライブラリ

# それぞれのフラグを'False'に設定
followme_flag = 'False'
speak_flag = 'False'
stop_flag = 'False'

# 類似度を計算する関数
def get_similar(listX, listY):
    s = difflib.SequenceMatcher(None, listX, listY).ratio()
    return s

def callback(data):
    global followme_flag
    global speak_flag
    global stop_flag
    request = data.data # 受け取った文字を'request'とおく
    print request
    # 類似度計算
    request_list = ['follow me', 'stop follow me', 'yes, i do', 'no i don\'t']
    max = 0
    answer = ''
    for q in request_list:
        level = get_similar(request, q)
        if level > max:
            max = level
            answer = q

    # ---ここから条件分岐の処理
    # ---Follow me
    if followme_flag != 'Finish':
        if answer == request_list[0]:  # 'follow me'と言われた時
            pub_stop.publish(False) # Falseを送り、音声認識ストップ
            followme_flag = 'True'  # followme_flagを'True'にする
            speak = 'Do you want me to start follow me?'  # 「follow me で正しいですか？」
            print speak
            pub_speak.publish(speak)  # speak発話
            wait("speak_flag")  # 発話が終わった合図が来るまで待つ
            pub_start.publish(True) # Trueを送り、音声認識再開

        # follow meが'Yes'の時の処理
        if (answer == request_list[2]) and (followme_flag == 'True'):
            pub_stop.publish(False)
            followme_flag = 'Finish'  # followme_flagを'Finish'にする
            stop_flag = 'False'
            speak = 'When we arrive at the destination, please say stop follow me.' # 「目的地に到着したらStop follow meと言ってください」
            print speak
            pub_speak.publish(speak)
            wait("speak_flag")
            # time.sleep(2) # 顔を覚えている（?）間の時間
            #pub.publish('start')  # 制御に'start'をpublish (***follow me 開始***)
            speak2 = 'I am ready to start follow me.'  # 「準備ができた」
            print speak2
            print('Follow me.......')
            pub_speak.publish(speak2)
            wait("speak_flag")
            pub_start.publish(True)
        # follow me が'No'の時の処理
        if (answer == request_list[3]) and (followme_flag == 'True'):
            pub_stop.publish(False)
            followme_flag = 'False'
            speak = 'OK. Please say the command again.' # 「もう一度お願いします」
            print speak
            pub_speak.publish(speak)
            wait("speak_flag")
            pub_start.publish(True)

    # ---Stop follow me
    if (followme_flag == 'Finish') and (stop_flag != 'Finish'):
        if answer == request_list[1]:
            pub_stop.publish(False)
            stop_flag = 'True'
            speak = 'Do you want me to stop follow me?'  # 「follow me で正しいですか？」
            print speak
            pub_speak.publish(speak)
            wait("speak_flag")
            pub_start.publish(True)

        # Stop follow meが'Yes'の時の処理
        if (answer == request_list[2]) and (stop_flag == 'True'):
            pub_stop.publish(False)
            stop_flag = 'Finish'
            followme_flag = 'False'
            #pub.publish('stop')  # 制御に'stop follow me'をpublish (***follow me停止***)
            speak = 'OK, I will stop. Please give me next command.'  # 「ストップします」
            print speak
            print('Stop follow me......')
            pub_speak.publish(speak)
            wait("speak_flag")
            pub_start.publish(True)

        # Stop follow meが'No'の時の処理
        if (answer == request_list[3]) and (stop_flag == 'True'):
            pub_stop.publish(False)
            stop_flag = 'False'
            speak = 'OK. Please say the command again.' # 「もう一度お願いします」
            print speak
            pub_speak.publish(speak)
            wait("speak_flag")
            pub_start.publish(True)


# 発話終了のメッセージを受け取る関数
def speak_signal(message):
    global speak_flag
    speak_flag = message.data
    print speak_flag

# speak_flagがspeaker'になるまでループし続ける関数
def wait(target):
    if target == "speak_flag":
        global speak_flag
        speak_flag = ''
    while (speak_flag != 'speaker'):
        pass


if __name__ == '__main__':
    #pub = rospy.Publisher('follow_me', String, queue_size=10) # 制御班にFollowme, Stopを送る
    pub_speak = rospy.Publisher('speaker', String, queue_size=10)  # 発話のノードに文字列を送る
    pub_start = rospy.Publisher('recognition_start', Bool, queue_size=10) # pocketsphinxの操作側に指示を送る(start)
    pub_stop = rospy.Publisher('recognition_stop', Bool, queue_size=10) # pocketsphinxの操作側に指示を送る(stop)
    rospy.Subscriber('finish', String, speak_signal)  # 発話が終わった指示を受けとる
    rospy.Subscriber('speech', String, callback) # 音声認識結果を受け取る
    rospy.init_node('help_listener', anonymous=True)
    rospy.spin()
